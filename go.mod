module gitlab.com/dwagin/go-json

go 1.23

require (
	github.com/stretchr/testify v1.10.0
	gitlab.com/dwagin/go-bytebuffer v1.4.6
	go.openly.dev/pointy v1.3.0
	golang.org/x/exp v0.0.0-20250207012021-f9890c6ad9f3
)

require (
	github.com/davecgh/go-spew v1.1.2-0.20180830191138-d8f796af33cc // indirect
	github.com/pmezard/go-difflib v1.0.1-0.20181226105442-5d4384ee4fb2 // indirect
	gopkg.in/yaml.v3 v3.0.1 // indirect
)
