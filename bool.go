package json

func AppendBool(dst []byte, b bool) []byte {
	if b {
		return append(dst, "true"...)
	}

	return append(dst, "false"...)
}
