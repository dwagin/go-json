package json_test

import (
	"net"
	"testing"
)

func (v *AnySuite) TestString() {
	testCases := []struct {
		Name   string
		Value  any
		Result string
	}{
		{
			"string",
			`\bla-"бла"-bla\<&>`,
			`"\\bla-\"бла\"-bla\\<&>"`,
		},
	}

	v.testList(testCases)
}

func (v *AnySuite) TestStringer() {
	testCases := []struct {
		Name   string
		Value  any
		Result string
	}{
		{
			"fmt.Stringer",
			net.IPv4(127, 0, 0, 1),
			`"127.0.0.1"`,
		},
	}

	v.testList(testCases)
}

func BenchmarkString(b *testing.B) {
	testCases := []struct { //nolint:govet // tests
		Name  string
		Value any
	}{
		{
			"string",
			`\bla-"бла"-bla\<&>`,
		},
	}

	benchList(b, testCases)
}

func BenchmarkStringer(b *testing.B) {
	testCases := []struct { //nolint:govet // tests
		Name  string
		Value any
	}{
		{
			"fmt.Stringer",
			net.IPv4(127, 0, 0, 1),
		},
	}

	benchList(b, testCases)
}
