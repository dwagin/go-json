package json_test

import (
	"math"
	"testing"
)

func (v *AnySuite) TestFloat() {
	testCases := []struct {
		Name   string
		Value  any
		Result string
	}{
		{
			"float32",
			float32(12345.11),
			`12345.11`,
		},
		{
			"float64",
			float64(-31283.45345392),
			`-31283.45345392`,
		},
		{
			"float64 NaN",
			math.NaN(),
			`NaN`,
		},
		{
			"float64 -Inf",
			math.Inf(-1),
			`-Inf`,
		},
		{
			"float64 +Inf",
			math.Inf(+1),
			`+Inf`,
		},
	}

	v.testList(testCases)
}

func BenchmarkFloat(b *testing.B) {
	testCases := []struct { //nolint:govet // tests
		Name  string
		Value any
	}{
		{
			"float32",
			float32(12345.11),
		},
		{
			"float64",
			float64(-31283.45345392),
		},
	}

	benchList(b, testCases)
}
