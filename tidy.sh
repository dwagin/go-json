#!/bin/sh

go get -u && go mod tidy && go mod vendor || exit 1

git add -A vendor
