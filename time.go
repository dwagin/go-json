package json

import (
	"time"
)

func AppendTime(dst []byte, t time.Time) []byte {
	dst = append(dst, Quote)
	dst = t.UTC().AppendFormat(dst, time.RFC3339Nano)
	dst = append(dst, Quote)

	return dst
}
