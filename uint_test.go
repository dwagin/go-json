package json_test

import (
	"testing"
)

func (v *AnySuite) TestUint() {
	testCases := []struct {
		Name   string
		Value  any
		Result string
	}{
		{
			"small uint #1",
			uint(9),
			`9`,
		},
		{
			"small uint #2",
			uint(99),
			`99`,
		},
		{
			"uint",
			uint(4294967295),
			`4294967295`,
		},
		{
			"uint8",
			uint8(255),
			`255`,
		},
		{
			"uint16",
			uint16(65535),
			`65535`,
		},
		{
			"uint32",
			uint32(4294967295),
			`4294967295`,
		},
		{
			"uint64",
			uint64(18446744073709551615),
			`18446744073709551615`,
		},
	}

	v.testList(testCases)
}

func BenchmarkUint(b *testing.B) {
	testCases := []struct { //nolint:govet // tests
		Name  string
		Value any
	}{
		{
			"4294967295",
			uint(4294967295),
		},
		{
			"9",
			uint(9),
		},
		{
			"99",
			uint(99),
		},
		{
			"999",
			uint(999),
		},
		{
			"9999",
			uint(9999),
		},
	}

	benchList(b, testCases)
}
