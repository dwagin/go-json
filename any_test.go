package json_test

import (
	ejson "encoding/json"
	"errors"
	"testing"

	"github.com/stretchr/testify/suite"
	"gitlab.com/dwagin/go-bytebuffer"
	"go.openly.dev/pointy"

	"gitlab.com/dwagin/go-json"
)

type (
	AppendJSON       int
	MarshalJSON      int
	MarshalJSONError int
	String           struct {
		Field int
	}
)

type AnySuite struct {
	suite.Suite
}

func (v *AnySuite) testList(
	testCases []struct {
		Name   string
		Value  any
		Result string
	},
) {
	for i := range testCases {
		test := testCases[i]

		v.Run(test.Name, func() {
			require := v.Require()

			bb := bytebuffer.Get()
			defer bb.Release()

			bb.B = json.Append(bb.B, test.Value)
			require.Equal(test.Result, string(bb.B))
		})
	}
}

func benchList(
	b *testing.B,
	testCases []struct { //nolint:govet // tests
		Name  string
		Value any
	},
) {
	b.Helper()

	for i := range testCases {
		test := testCases[i]
		b.Run(test.Name, func(b *testing.B) {
			bb := bytebuffer.Get()
			defer bb.Release()

			b.ReportAllocs()
			b.ResetTimer()

			for range b.N {
				bb.Reset()
				bb.B = json.Append(bb.B, test.Value)
			}
		})
	}
}

func (v AppendJSON) AppendJSON(dst []byte) []byte {
	return append(dst, `"type AppendJSON"`...)
}

func (v AppendJSON) String() string {
	panic("AppendJSON() should be preferred over String()")
}

func (v MarshalJSON) MarshalJSON() ([]byte, error) {
	return []byte(`"type MarshalJSON"`), nil
}

func (v MarshalJSONError) MarshalJSON() ([]byte, error) {
	return nil, errors.New("json.Marshaler error")
}

func (v String) String() string {
	return "type String"
}

func TestAny(t *testing.T) {
	suite.Run(t, new(AnySuite))
}

func (v *AnySuite) TestSpecial() {
	testCases := []struct {
		Name   string
		Value  any
		Result string
	}{
		{
			"nil",
			nil,
			`null`,
		},
		{
			"Appender",
			AppendJSON(999),
			`"type AppendJSON"`,
		},
		{
			"Pointer to Appender",
			pointy.Pointer(AppendJSON(998)),
			`"type AppendJSON"`,
		},
		{
			"json.RawMessage",
			ejson.RawMessage(`{"a":1,"b":2}`),
			`{"a":1,"b":2}`,
		},
		{
			"json.Marshaler",
			MarshalJSON(-999),
			`"type MarshalJSON"`,
		},
		{
			"Pointer to json.Marshaler",
			pointy.Pointer(MarshalJSON(-998)),
			`"type MarshalJSON"`,
		},
		{
			"json.Marshaler error",
			MarshalJSONError(999),
			`"json: error calling MarshalJSON for type json_test.MarshalJSONError: json.Marshaler error"`,
		},
		{
			"fmt.Stringer",
			String{Field: -999},
			`"type String"`,
		},
		{
			"Pointer to fmt.Stringer",
			pointy.Pointer(String{Field: -998}),
			`"type String"`,
		},
	}

	v.testList(testCases)
}

func BenchmarkAny(b *testing.B) {
	testCases := []struct { //nolint:govet // tests
		Name  string
		Value any
	}{
		{
			"json.Marshaler",
			MarshalJSON(-999),
		},
		{
			"json.Marshaler error",
			MarshalJSONError(999),
		},
	}

	benchList(b, testCases)
}
