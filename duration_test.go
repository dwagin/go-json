package json_test

import (
	"testing"
	"time"

	"gitlab.com/dwagin/go-bytebuffer"

	"gitlab.com/dwagin/go-json"
)

func (v *AnySuite) TestDuration() {
	testCases := []struct {
		Name   string
		Value  any
		Result string
	}{
		{
			"0s",
			time.Duration(0),
			`"0s"`,
		},
		{
			"1ns",
			1 * time.Nanosecond,
			`"1ns"`,
		},
		{
			"1.1µs",
			1100 * time.Nanosecond,
			`"1.1µs"`,
		},
		{
			"2.2ms",
			2200 * time.Microsecond,
			`"2.2ms"`,
		},
		{
			"3.3s",
			3300 * time.Millisecond,
			`"3.3s"`,
		},
		{
			"4m5s",
			4*time.Minute + 5*time.Second,
			`"4m5s"`,
		},
		{
			"4m5.001s",
			4*time.Minute + 5001*time.Millisecond,
			`"4m5.001s"`,
		},
		{
			"5h6m7.001s",
			5*time.Hour + 6*time.Minute + 7001*time.Millisecond,
			`"5h6m7.001s"`,
		},
		{
			"8m0.000000001s",
			8*time.Minute + 1*time.Nanosecond,
			`"8m0.000000001s"`,
		},
		{
			"2562047h47m16.854775807s",
			time.Duration(1<<63 - 1),
			`"2562047h47m16.854775807s"`,
		},
		{
			"1h0m0.000000001s",
			1*time.Hour + 1*time.Nanosecond,
			`"1h0m0.000000001s"`,
		},
		{
			"-100ns",
			-100 * time.Nanosecond,
			`"-100ns"`,
		},
		{
			"-1h0m0.000000001s",
			-1*time.Hour - 1*time.Nanosecond,
			`"-1h0m0.000000001s"`,
		},
		{
			"-2562047h47m16.854775808s",
			time.Duration(-1 << 63),
			`"-2562047h47m16.854775808s"`,
		},
	}

	for i := range testCases {
		test := testCases[i]

		v.Run(test.Name, func() {
			require := v.Require()

			bb := bytebuffer.Get()
			defer bb.Release()

			bb.B = json.Append(bb.B, test.Value)
			//nolint:forcetypeassert,errcheck // test
			require.Equal(`"`+test.Value.(time.Duration).String()+`"`, string(bb.B))
			require.Equal(test.Result, string(bb.B))
		})
	}
}

func BenchmarkDuration(b *testing.B) {
	d := 99*time.Hour + 59*time.Minute + 59*time.Second + 123456789

	b.Run("Old", func(b *testing.B) {
		bb := bytebuffer.Get()
		defer bb.Release()

		b.ReportAllocs()
		b.ResetTimer()

		for range b.N {
			bb.Reset()
			bb.B = append(bb.B, json.Quote)
			bb.B = append(bb.B, d.String()...)
			bb.B = append(bb.B, json.Quote)
		}
	})

	b.Run("New", func(b *testing.B) {
		bb := bytebuffer.Get()
		defer bb.Release()

		b.ReportAllocs()
		b.ResetTimer()

		for range b.N {
			bb.Reset()
			bb.B = json.AppendDuration(bb.B, d)
		}
	})
}
