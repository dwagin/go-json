package json_test

import (
	"testing"
	"time"
)

func (v *AnySuite) TestSliceAny() {
	testCases := []struct {
		Name   string
		Value  any
		Result string
	}{
		{
			"nil",
			[]any(nil),
			`null`,
		},
		{
			"empty",
			[]any{},
			`[]`,
		},
		{
			"many",
			[]any{"string<&>", 42},
			`["string<&>",42]`,
		},
	}

	v.testList(testCases)
}

func (v *AnySuite) TestSliceBool() {
	testCases := []struct {
		Name   string
		Value  any
		Result string
	}{
		{
			"nil",
			[]bool(nil),
			`null`,
		},
		{
			"empty",
			[]bool{},
			`[]`,
		},
		{
			"many",
			[]bool{true, true, false, true},
			`[true,true,false,true]`,
		},
	}

	v.testList(testCases)
}

func (v *AnySuite) TestSliceString() {
	testCases := []struct {
		Name   string
		Value  any
		Result string
	}{
		{
			"nil",
			[]string(nil),
			`null`,
		},
		{
			"empty",
			[]string{},
			`[]`,
		},
		{
			"many",
			[]string{`bla-bla-bla`, `bla-бла-bla`, `\bla-"бла"-bla\<&>`},
			`["bla-bla-bla","bla-бла-bla","\\bla-\"бла\"-bla\\<&>"]`,
		},
	}

	v.testList(testCases)
}

func (v *AnySuite) TestSliceInt() {
	testCases := []struct {
		Name   string
		Value  any
		Result string
	}{
		{
			"nil []int",
			[]int(nil),
			`null`,
		},
		{
			"empty []int",
			[]int{},
			`[]`,
		},
		{
			"many []int",
			[]int{-9, -8, -7, -6, -5, -4, -3, -2, -1, 0, 1, 2, 3, 4, 5, 6, 7, 9},
			`[-9,-8,-7,-6,-5,-4,-3,-2,-1,0,1,2,3,4,5,6,7,9]`,
		},
		{
			"many []int8",
			[]int8{-128, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 127},
			`[-128,0,1,2,3,4,5,6,7,8,9,127]`,
		},
		{
			"many []int16",
			[]int16{-32768, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 32767},
			`[-32768,0,1,2,3,4,5,6,7,8,9,32767]`,
		},
		{
			"many []int32",
			[]int32{-2147483648, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 2147483647},
			`[-2147483648,0,1,2,3,4,5,6,7,8,9,2147483647]`,
		},
		{
			"many []int64",
			[]int64{-9223372036854775808, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 9223372036854775807},
			`[-9223372036854775808,0,1,2,3,4,5,6,7,8,9,9223372036854775807]`,
		},
	}

	v.testList(testCases)
}

func (v *AnySuite) TestSliceUint() {
	testCases := []struct {
		Name   string
		Value  any
		Result string
	}{
		{
			"[]uint nil",
			[]uint(nil),
			`null`,
		},
		{
			"[]uint empty",
			[]uint{},
			`[]`,
		},
		{
			"[]uint many",
			[]uint{0, 1, 2, 3, 4, 5, 6, 7, 9},
			`[0,1,2,3,4,5,6,7,9]`,
		},
		{
			"[]uint8 many",
			[]uint8{0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 128},
			`[0,1,2,3,4,5,6,7,8,9,128]`,
		},
		{
			"[]uint16 many",
			[]uint16{0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 32768},
			`[0,1,2,3,4,5,6,7,8,9,32768]`,
		},
		{
			"[]uint32 many",
			[]uint32{0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 2147483648},
			`[0,1,2,3,4,5,6,7,8,9,2147483648]`,
		},
		{
			"[]uint64 many",
			[]uint64{0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 9223372036854775808},
			`[0,1,2,3,4,5,6,7,8,9,9223372036854775808]`,
		},
	}

	v.testList(testCases)
}

func (v *AnySuite) TestSliceFloat() {
	testCases := []struct {
		Name   string
		Value  any
		Result string
	}{
		{
			"nil []float32",
			[]float32(nil),
			`null`,
		},
		{
			"empty []float32",
			[]float32{},
			`[]`,
		},
		{
			"many []float32",
			[]float32{12345.11, 0, 1, 2, 3, 4, 5, 6, 7, 9},
			`[12345.11,0,1,2,3,4,5,6,7,9]`,
		},
		{
			"nil []float64",
			[]float64(nil),
			`null`,
		},
		{
			"empty []float64",
			[]float64{},
			`[]`,
		},
		{
			"many []float64",
			[]float64{-31283.45345392, 0, 1, 2, 3, 4, 5, 6, 7, 9},
			`[-31283.45345392,0,1,2,3,4,5,6,7,9]`,
		},
	}

	v.testList(testCases)
}

func (v *AnySuite) TestSliceDuration() {
	testCases := []struct {
		Name   string
		Value  any
		Result string
	}{
		{
			"nil []time.Duration",
			[]time.Duration(nil),
			`null`,
		},
		{
			"empty []time.Duration",
			[]time.Duration{},
			`[]`,
		},
		{
			"many []time.Duration",
			[]time.Duration{
				time.Duration(0),
				1 * time.Nanosecond,
				1100 * time.Nanosecond,
				2200 * time.Microsecond,
				3300 * time.Millisecond,
				4*time.Minute + 5*time.Second,
				4*time.Minute + 5001*time.Millisecond,
				5*time.Hour + 6*time.Minute + 7001*time.Millisecond,
				8*time.Minute + 1*time.Nanosecond,
			},
			`["0s","1ns","1.1µs","2.2ms","3.3s","4m5s","4m5.001s","5h6m7.001s","8m0.000000001s"]`,
		},
	}

	v.testList(testCases)
}

func (v *AnySuite) TestSliceTime() {
	testCases := []struct {
		Name   string
		Value  any
		Result string
	}{
		{
			"nil []time.Time",
			[]time.Time(nil),
			`null`,
		},
		{
			"empty []time.Time",
			[]time.Time{},
			`[]`,
		},
		{
			"many []time.Time",
			[]time.Time{
				time.Unix(0, 0),
				time.Unix(1, 0),
				time.Unix(2, 0),
				time.Unix(3, 0),
				time.Unix(4, 0),
				time.Unix(5, 0),
				time.Unix(6, 0),
				time.Unix(7, 0),
				time.Unix(8, 0),
				time.Unix(9, 0),
			},
			`["1970-01-01T00:00:00Z","1970-01-01T00:00:01Z","1970-01-01T00:00:02Z","1970-01-01T00:00:03Z","1970-01-01T00:00:04Z","1970-01-01T00:00:05Z","1970-01-01T00:00:06Z","1970-01-01T00:00:07Z","1970-01-01T00:00:08Z","1970-01-01T00:00:09Z"]`, //nolint:lll // test
		},
	}

	v.testList(testCases)
}

func BenchmarkSlice(b *testing.B) {
	testCases := []struct { //nolint:govet // tests
		Name  string
		Value any
	}{
		{
			"[]any #1",
			[]any{"string<&>", 42},
		},
		{
			"[]any #2",
			[]any{0, 1, 2, 3, 4, 5, 6, 7, 8, 9, uint64(9223372036854775808)},
		},
		{
			"[]uint64",
			[]uint64{0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 9223372036854775808},
		},
	}

	benchList(b, testCases)
}
