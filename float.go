package json

import (
	"strconv"
)

func AppendFloat32(dst []byte, src float32) []byte {
	return strconv.AppendFloat(dst, float64(src), 'g', -1, 32)
}

func AppendFloat64(dst []byte, src float64) []byte {
	return strconv.AppendFloat(dst, src, 'g', -1, 64)
}
