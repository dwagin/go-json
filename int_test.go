package json_test

import (
	"testing"
)

func (v *AnySuite) TestInt() {
	testCases := []struct {
		Name   string
		Value  any
		Result string
	}{
		{
			"small int #1",
			int(9),
			`9`,
		},
		{
			"small int #2",
			int(99),
			`99`,
		},
		{
			"int",
			int(-2147483648),
			`-2147483648`,
		},
		{
			"int8 #1",
			int8(-128),
			`-128`,
		},
		{
			"int8 #2",
			int8(127),
			`127`,
		},
		{
			"int16 #1",
			int16(-32768),
			`-32768`,
		},
		{
			"int16 #2",
			int16(32767),
			`32767`,
		},
		{
			"int32 #1",
			int32(-2147483648),
			`-2147483648`,
		},
		{
			"int32 #2",
			int32(2147483647),
			`2147483647`,
		},
		{
			"int64 #1",
			int64(-9223372036854775808),
			`-9223372036854775808`,
		},
		{
			"int64 #2",
			int64(9223372036854775807),
			`9223372036854775807`,
		},
	}

	v.testList(testCases)
}

func BenchmarkInt(b *testing.B) {
	testCases := []struct { //nolint:govet // tests
		Name  string
		Value any
	}{
		{
			"-2147483648",
			int(-2147483648),
		},
		{
			"9",
			int(9),
		},
		{
			"99",
			int(99),
		},
		{
			"999",
			int(999),
		},
		{
			"9999",
			int(9999),
		},
	}

	benchList(b, testCases)
}
