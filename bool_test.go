package json_test

func (v *AnySuite) TestBool() {
	testCases := []struct {
		Name   string
		Value  any
		Result string
	}{
		{
			"true",
			true,
			`true`,
		},
		{
			"false",
			false,
			`false`,
		},
	}

	v.testList(testCases)
}
