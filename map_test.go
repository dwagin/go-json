package json_test

import (
	"testing"
)

func (v *AnySuite) TestMapAny() {
	testCases := []struct {
		Name   string
		Value  any
		Result string
	}{
		{
			"nil",
			map[string]any(nil),
			`null`,
		},
		{
			"empty",
			map[string]any{},
			`{}`,
		},
		{
			"many",
			map[string]any{
				"A": "string<&>",
				"B": 42,
			},
			`{"A":"string<&>","B":42}`,
		},
	}

	v.testList(testCases)
}

func BenchmarkMap(b *testing.B) {
	testCases := []struct { //nolint:govet // tests
		Name  string
		Value any
	}{
		{
			"Struct",
			struct {
				A string
				B int
			}{
				A: "string<&>",
				B: 42,
			},
		},
		{
			"Map",
			map[string]any{
				"A": "string<&>",
				"B": 42,
			},
		},
	}

	benchList(b, testCases)
}
