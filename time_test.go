package json_test

import (
	"testing"
	"time"
)

func (v *AnySuite) TestTime() {
	testCases := []struct {
		Name   string
		Value  any
		Result string
	}{
		{
			"zero",
			time.Time{}.UTC(),
			`"0001-01-01T00:00:00Z"`,
		},
	}

	v.testList(testCases)
}

func BenchmarkTime(b *testing.B) {
	testCases := []struct { //nolint:govet // tests
		Name  string
		Value any
	}{
		{
			"time.Time",
			time.Time{}.UTC(),
		},
	}

	benchList(b, testCases)
}
